//
// Created by admin on 03.11.17.
//

#import <Foundation/Foundation.h>
#import "OVKApi.h"

@interface OVKFriendsFetcher : NSObject
+ (id)sharedFetcher;
- (BOOL)hasMorePages;
- (NSInteger)getCountOfFriends;
- (void)loadNextPageWithSuccessBlock:(void (^)(NSMutableArray<OVKUser *> *loadedFriends))success andFailureBlock:(void (^)(NSError *error))failure;
- (OVKUser *)getFriendsAtIndex:(NSInteger)index;
- (void)resetCurrentPage;
@end
