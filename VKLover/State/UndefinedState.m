//
//  UndefinedState.m
//  VKLover
//
//  Created by admin on 13.11.17.
//

#import "UndefinedState.h"

@implementation UndefinedState
- (id)init{
    if (self = [super init]) {
    }
    self.color = [UIColor colorWithRed:0.73 green:0.73 blue:0.73 alpha:1.0];
    return self;
}
@synthesize color;

@end
