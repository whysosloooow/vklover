//
//  LoginViewController.m
//  VKLover
//
//  Created by admin on 03.11.17.
//

#import "OVKLoginViewController.h"
#import "OVKCurrentUser.h"
#import "OVKApi.h"
#import "OVKFriendsViewController.h"
#import "OVKFriendsFetcher.h"
#import "LikesManager.h"
#import "MBProgressHUD.h"

@interface OVKLoginViewController ()

@end

@implementation OVKLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [OVKApi sharedApi];
    [[VKSdk instance] setUiDelegate:self];
  /*  [[OVKApi sharedApi] wakeUpSessionWithSuccess:^(VKAuthorizationState state) {
        [self tryToWakeUpSession];
    } and:^(NSError *error) {
        NSLog(@"Error - %@", [error localizedDescription]);
    }];*/
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (IBAction)loginAction:(UIButton *)sender {
    [self tryToWakeUpSession];
    [[OVKApi sharedApi] authorizeWithSuccess:^(OVKCurrentUser *currentUser) {
        [currentUser saveToUserDefaults];
        //[self changeRootViewController];
    } andFailure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        //alert with connection troubles
    }];
}

-(void)changeRootViewController {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *navVC = [storyboard instantiateViewControllerWithIdentifier:@"mainNavigation"];
    [UIView transitionFromView:UIApplication.sharedApplication.delegate.window.rootViewController.view
                        toView:navVC.view
                      duration:1
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    completion:^(BOOL finished) {
                        if(finished) {
                            NSLog(@"ANIMATION FINISHED!");
                            UIApplication.sharedApplication.delegate.window.rootViewController = navVC;
                        }
                    }];
}

-(void)tryToWakeUpSession {
    [[OVKApi sharedApi] wakeUpSessionWithSuccess:^(VKAuthorizationState state) {
        OVKCurrentUser* user = [OVKCurrentUser loadCurrentUser];
        if(user.token && state == VKAuthorizationAuthorized) {
            [self loadLikesAndDislikesWithCompletion:^(BOOL success) {
               [self changeRootViewController];
            }];
            
        } else {
            NSLog(@"no user or token in UserDefaults");
        }
    } and:^(NSError *error) {
        NSLog(@"Error - %@", [error localizedDescription]);
    }];
}

- (void)loadLikesAndDislikesWithCompletion:(void (^) (BOOL success))completion {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.label.text = @"Загрузка";
    [[LikesManager sharedInstance] loadLikesWithCompletion:^(BOOL success) {
        [[LikesManager sharedInstance] loadDislikesWithCompletion:^(BOOL success) {
            NSLog(@"finished loading likes/dislikes");
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (completion) {
                completion(YES);
            }
        }];
    }];
}

#pragma mark: -VKSdkUIDelegate
- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    VKCaptchaViewController* vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:self];
}

- (void)vkSdkDidDismissViewController:(UIViewController *)controller {
    if([OVKCurrentUser loadCurrentUser].token) {
        [self loadLikesAndDislikesWithCompletion:^(BOOL success) {
            [self changeRootViewController];
        }];
    }
}

@end
