//
//  RatedFriendsViewCell.m
//  VKLover
//
//  Created by admin on 14.11.17.
//

#import "RatedFriendsViewCell.h"

@implementation RatedFriendsViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self layoutIfNeeded];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.coloredCircleBackgroundView.layer.cornerRadius = self.coloredCircleBackgroundView.frame.size.width / 2;
    self.friendImage.layer.cornerRadius = self.friendImage.frame.size.width / 2;

}
@end
