//
//  OVKUser.h
//  VKLover
//
//  Created by admin on 02.11.17.
//

#import <Foundation/Foundation.h>
#import <Motis.h>
#import "OVKState.h"

@interface OVKUser : NSObject

@property (strong, nonatomic) NSString* first_name;
@property (strong, nonatomic) NSString* last_name;
@property (strong, nonatomic) NSString* photo_200;
@property (strong, nonatomic) NSNumber* sex;
@property (strong, nonatomic) NSNumber* user_id;
@property (strong, nonatomic) NSString* city;
@property (strong, nonatomic) NSString* birthday;
@property (strong, nonatomic) id <OVKState> state;
@property (strong, nonatomic) NSString* last_seen;
+ (NSMutableArray*)createUsersWithResponseItems:(NSArray*)items;

@end

