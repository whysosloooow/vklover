//
//  AppDelegate.h
//  VKLover
//
//  Created by admin on 01.11.17.
//

#import <UIKit/UIKit.h>
#import <VK-ios-sdk/VKSdk.h>
#import <FirebaseMessaging/FirebaseMessaging.h>
#import <FirebaseInstanceID/FirebaseInstanceID.h>
#import <UserNotifications/UserNotifications.h>
@import Firebase;

@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate, FIRMessagingDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

