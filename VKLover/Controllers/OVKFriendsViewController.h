//
//  FriendsViewController.h
//  VKLover
//
//  Created by admin on 03.11.17.
//

#import <UIKit/UIKit.h>
#import <VK-ios-sdk/VKSdk.h>
#import <MDCSwipeToChoose/MDCSwipeToChoose.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <FirebaseMessaging/FirebaseMessaging.h>
#import <FirebaseInstanceID/FirebaseInstanceID.h>

@interface OVKFriendsViewController : UIViewController <MDCSwipeToChooseDelegate>

@end
