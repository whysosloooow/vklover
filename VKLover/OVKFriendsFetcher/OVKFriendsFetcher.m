//
// Created by admin on 03.11.17.
//

#import "OVKFriendsFetcher.h"
#import "OVKUser.h"
#import "LikesManager.h"

@interface OVKFriendsFetcher()
@end

@implementation OVKFriendsFetcher {
    NSInteger _currentPage;
    OVKFriendsFetcher *sharedFetcher;
    NSArray *_loadedFriends;
    BOOL _hasMorePages;
}

+(id)sharedFetcher {
    static dispatch_once_t once;
    static OVKFriendsFetcher *sharedFetcher = nil;
    dispatch_once(&once, ^{
        sharedFetcher = [[OVKFriendsFetcher alloc] init];
    });
    return sharedFetcher;
}

- (id)init{
    if (self = [super init]) {
        _currentPage = 0;
        _hasMorePages = YES;
        _loadedFriends = [[NSArray alloc] init];
    }
    return self;
}

- (BOOL)hasMorePages {
    return _hasMorePages;
}

- (NSInteger)getCountOfFriends {
    return [_loadedFriends count];
}

- (void)loadNextPageWithSuccessBlock:(void (^)(NSArray<OVKUser *> *loadedFriends))success andFailureBlock:(void (^)(NSError *error))failure {
    _loadedFriends = [[NSArray alloc] init];

    [self loadPageWithSuccessBlock:success andFailureBlock:failure];
}



- (void)loadPageWithSuccessBlock:(void (^)(NSArray<OVKUser *> *loadedFriends))success andFailureBlock:(void (^)(NSError *error))failure {
    [[OVKApi sharedApi] getFriendsWithPage:_currentPage
                          withSuccessBlock:^(NSMutableArray<OVKUser *> *friends, BOOL hasPages) {
                              _currentPage++;
                              if (!hasPages) {
                                  _hasMorePages = NO;
                                  if (success) {
                                      success(nil);
                                  }
                              } else {
                                  _loadedFriends = [[friends filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(OVKUser *evaluatedObject, NSDictionary *bindings) {
                                      return !([[[LikesManager sharedInstance] likes] member:[evaluatedObject.user_id stringValue]] ||
                                              [[[LikesManager sharedInstance] dislikes] member:[evaluatedObject.user_id stringValue]]);
                                  }]] arrayByAddingObjectsFromArray:_loadedFriends];
                                  if (success && [_loadedFriends count] >= 10) {
                                      success([_loadedFriends copy]);
                                  } else {
                                      [self loadPageWithSuccessBlock:success andFailureBlock:failure];
                                  }
                              }
                          }
                           andFailureBlock:^(NSError *error) {
                               if (failure) {
                                   failure(error);
                               }
                           }];
}

- (OVKUser *)getFriendsAtIndex:(NSInteger)index {
    if ((_loadedFriends) && index < [self getCountOfFriends]) {
        return _loadedFriends[index];
    } else {
        return nil;
    }
}

- (void)resetCurrentPage {
    _currentPage = 0;
}


@end
