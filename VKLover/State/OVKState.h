//
//  OVKState.h
//  VKLover
//
//  Created by admin on 13.11.17.
//

#import <UIKit/UIKit.h>

#ifndef OVKState_h
#define OVKState_h
@protocol OVKState <NSObject>
@property UIColor* color;
@end
#endif /* OVKState_h */
