//
//  OVKLaunchViewController.m
//  VKLover
//
//  Created by admin on 23.11.17.
//

#import "OVKLaunchViewController.h"
#import "LikesManager.h"
#import "OVKLoginViewController.h"
#import "OVKApi.h"

@interface OVKLaunchViewController ()

@end

@implementation OVKLaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self tryToWakeUpSession];
}

-(void)tryToWakeUpSession {
    [[OVKApi sharedApi] wakeUpSessionWithSuccess:^(VKAuthorizationState state) {
        OVKCurrentUser* user = [OVKCurrentUser loadCurrentUser];
        if(user.token && state == VKAuthorizationAuthorized) {
            //[self loadLikesAndDislikesWithCompletion:^(BOOL success) {
                [self changeRootViewControllerTo:YES];
            //}];
        } else {
            NSLog(@"no user or token in UserDefaults");
            [self changeRootViewControllerTo:NO];
        }
    } and:^(NSError *error) {
        NSLog(@"Error - %@", [error localizedDescription]);
    }];
}

- (void)loadLikesAndDislikesWithCompletion:(void (^) (BOOL success))completion {
    [[LikesManager sharedInstance] loadLikesWithCompletion:^(BOOL success) {
        [[LikesManager sharedInstance] loadDislikesWithCompletion:^(BOOL success) {
            NSLog(@"finished loading likes/dislikes");
            if (completion) {
                completion(YES);
            }
        }];
    }];
}

-(void)changeRootViewControllerTo:(BOOL)freinds {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if(freinds) {
        UIViewController *tabVC = [storyboard instantiateViewControllerWithIdentifier:@"mainNavigation"];
        UIApplication.sharedApplication.delegate.window.rootViewController = tabVC;
    } else {
        UIViewController *loginVC = [storyboard instantiateViewControllerWithIdentifier:@"login_vc_id"];
        UIApplication.sharedApplication.delegate.window.rootViewController = loginVC;
    }
}

@end
