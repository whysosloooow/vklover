//
//  TGStyleImage.h
//  VKLover
//
//  Created by admin on 20.11.17.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface TGStyleImageView : UIImageView
-(void)updateView;
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;
@property (nonatomic, assign) IBInspectable BOOL round;
@property (nonatomic, assign) IBInspectable CGFloat borderWidth;
@property (nonatomic, strong) IBInspectable UIColor * borderColor;
@end

