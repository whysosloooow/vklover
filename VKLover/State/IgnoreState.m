//
//  IgnoreState.m
//  VKLover
//
//  Created by admin on 13.11.17.
//

#import "IgnoreState.h"

@implementation IgnoreState
- (id)init{
    if (self = [super init]) {
    }
    self.color = [UIColor colorWithRed:0.78 green:0.26 blue:0.26 alpha:1.0];
    return self;
}
@synthesize color;

@end
