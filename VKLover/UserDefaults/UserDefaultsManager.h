//
//  UserDefaultsManager.h
//  VKLover
//
//  Created by admin on 02.11.17.
//

#import <Foundation/Foundation.h>
#import "OVKCurrentUser.h"

@interface UserDefaultsManager : NSObject

+ (void)saveCustomObject:(id)encodedObject key:(NSString *)key;
+ (id)loadCustomObjectWithKey:(NSString *)key;
+ (void)removeObjectWithKey:(NSString*)key;

@end

