//
//  OVKApi.h
//  iosvksdktest
//
//  Created by admin on 01.11.17.
//

#import <Foundation/Foundation.h>
#import <VK-ios-sdk/VKSdk.h>
#import "OVKCurrentUser.h"
#import "UserDefaultsManager.h"

typedef void (^OVKgetFriendsSuccess)(NSMutableArray<OVKUser *>* friends, BOOL hasPages);
typedef void (^OVKFailure)(NSError* error);
typedef void (^OVKwakeUpSessionSucces)(VKAuthorizationState state);


@interface OVKApi : NSObject
+ (OVKApi *)sharedApi;
- (void)wakeUpSessionWithSuccess:(OVKwakeUpSessionSucces)success and:(OVKFailure)failure;
- (void)getFriendsWithPage:(NSInteger)page withSuccessBlock:(OVKgetFriendsSuccess)success andFailureBlock:(OVKFailure)failure;
- (void)authorizeWithSuccess:(void(^)(OVKCurrentUser *currentUser))success andFailure:(void(^)(NSError *error))failure;
- (void)getPersonalDataWithSuccessBlock:(void(^)(NSDictionary *currentUser))success andFailureBlock:(void(^)(NSError *error))failure;
- (void)getUsersbyIdsArray:(NSArray *)idsFromFB WithSuccessBlock:(void(^)(NSArray<OVKUser *> *loadedUsers))success andFailureBlock:(void(^)(NSError *error))failure;
@end
