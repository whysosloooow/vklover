//
//  OVKCurrentUser.m
//  VKLover
//
//  Created by admin on 02.11.17.
//

#import "OVKCurrentUser.h"
#import "UserDefaultsManager.h"

#define CURRENT_USER @"CURRENT_USER"

@implementation OVKCurrentUser

- (id)initWithDictionary:(NSDictionary*)dict andToken:(NSString*)token {
    self = [super init];
    if (self) {
        [self mts_setValuesForKeysWithDictionary:dict];
        _token = token;
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.first_name forKey:@"first_name"];
    [encoder encodeObject:self.last_name forKey:@"last_name"];
    [encoder encodeObject:self.photo_200 forKey:@"photo_200"];
    [encoder encodeObject:self.sex forKey:@"sex"];
    [encoder encodeObject:self.user_id forKey:@"user_id"];
    [encoder encodeObject:self.city forKey:@"city"];
    [encoder encodeObject:self.token forKey:@"token"];
    [encoder encodeObject:self.birthday forKey:@"birthday"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.first_name = [decoder decodeObjectForKey:@"first_name"];
        self.last_name = [decoder decodeObjectForKey:@"last_name"];
        self.photo_200 = [decoder decodeObjectForKey:@"photo_200"];
        self.sex = [decoder decodeObjectForKey:@"sex"];
        self.user_id = [decoder decodeObjectForKey:@"user_id"];
        self.city = [decoder decodeObjectForKey:@"city"];
        self.token = [decoder decodeObjectForKey:@"token"];
        self.birthday = [decoder decodeObjectForKey:@"birthday"];
    }
    return self;
}

+ (NSDictionary*)mts_mapping
{
    return @{@"first_name": mts_key(first_name),
             @"last_name": mts_key(last_name),
             @"photo_200": mts_key(photo_200),
             @"sex": mts_key(sex),
             @"id": mts_key(user_id),
             @"city.title": mts_key(city),
             @"bdate": mts_key(birthday)
             };
}

- (void)saveToUserDefaults {
    [UserDefaultsManager saveCustomObject:self key:CURRENT_USER];
}

+ (OVKCurrentUser*)loadCurrentUser {
    return [UserDefaultsManager loadCustomObjectWithKey:CURRENT_USER];
}

+ (void)removeCurrentUser {
    [UserDefaultsManager removeObjectWithKey:CURRENT_USER];
}

@end
