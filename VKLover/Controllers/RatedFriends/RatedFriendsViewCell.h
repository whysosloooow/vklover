//
//  RatedFriendsViewCell.h
//  VKLover
//
//  Created by admin on 14.11.17.
//

#import <UIKit/UIKit.h>
#import "TGStyleImageView.h"

@interface RatedFriendsViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet TGStyleImageView *friendImage;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *lastSeenLabel;
@property (strong, nonatomic) IBOutlet UIView *coloredCircleBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *avatarView;
@property (strong, nonatomic) IBOutlet UILabel *isAppUserLabel;
@end
