//
//  OVKCurrentUser.h
//  VKLover
//
//  Created by admin on 02.11.17.
//

#import "OVKUser.h"
#import <VK-ios-sdk/VKSdk.h>
#import <Motis.h>

@interface OVKCurrentUser : OVKUser <NSCoding>

@property(strong, nonatomic) NSString* token;

- (id)initWithDictionary:(NSDictionary*)dict andToken:(NSString*)token;
- (void)saveToUserDefaults;
+ (OVKCurrentUser*)loadCurrentUser;
+ (void)removeCurrentUser;

@end

