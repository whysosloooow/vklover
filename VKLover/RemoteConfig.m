//
//  RemoteConfig.m
//  VKLover
//
//  Created by admin on 09.11.17.
//

#import "RemoteConfig.h"
#import "OVKCurrentUser.h"

@interface RemoteConfig ()

@end

@implementation RemoteConfig

- (instancetype)init
{
    self = [super init];
    if (self) {
        //нужен файл настроек
       // [[FIRRemoteConfig remoteConfig] setDefaultsFromPlistFileName:@"Settings"];
    }
    return self;
}

+(id)sharedInstance {
    static RemoteConfig *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

@end
