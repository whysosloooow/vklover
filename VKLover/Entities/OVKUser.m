//
//  OVKUser.m
//  VKLover
//
//  Created by admin on 02.11.17.
//

#import "OVKUser.h"
#import "LikeState.h"
#import "IgnoreState.h"
#import "UndefinedState.h"
#import "LikesManager.h"

@implementation OVKUser

+ (NSDictionary*)mts_mapping
{
    return @{@"first_name": mts_key(first_name),
             @"last_name": mts_key(last_name),
             @"photo_200": mts_key(photo_200),
             @"sex": mts_key(sex),
             @"id": mts_key(user_id),
             @"city.title": mts_key(city),
             @"bdate": mts_key(birthday),
             @"last_seen.time": mts_key(last_seen)
             };
}

+ (NSMutableArray*)createUsersWithResponseItems:(NSArray*)items {
    NSMutableArray* returnFriends = [[NSMutableArray alloc] init];
    if(items) {
        for(NSDictionary* item in items) {
            OVKUser* newUser = [[OVKUser alloc] init];
            [newUser mts_setValuesForKeysWithDictionary:item];
            
            if([newUser.first_name length] == 0) {
                break;
            }
            if([newUser.last_name length] == 0) {
                break;
            }
            if([newUser.city length] == 0) {
                newUser.city = @"";
            }
            if ([newUser.birthday length] == 0) {
                newUser.birthday = @"";
            }
            
            if ([[LikesManager sharedInstance] isLikedUserWithId:[newUser.user_id integerValue]]) {
                newUser.state = [[LikeState alloc] init];
            } else if ([[LikesManager sharedInstance] isDislikedUserWithId:[newUser.user_id integerValue]]) {
                newUser.state = [[IgnoreState alloc] init];
            } else {
                newUser.state = [[UndefinedState alloc] init];
            }
            
            NSTimeInterval _interval=[newUser.last_seen doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.locale = [NSLocale currentLocale];
            formatter.timeStyle = NSDateFormatterShortStyle;
            formatter.dateStyle = NSDateFormatterShortStyle;
            formatter.doesRelativeDateFormatting = YES;
            newUser.last_seen = [formatter stringFromDate:date];
            
            if((newUser.user_id) && (newUser.photo_200)) {
                [returnFriends addObject:newUser];
            } else {
                NSLog(@"No avatar or user_id properties");
                //установить аватар по умолчанию
                newUser.photo_200 = @"https://pp.userapi.com/c837233/v837233574/5b869/oH6QgS5Wsak.jpg";
            }
        }
    } else {
        NSLog(@"error - no items in array");
    }
    return returnFriends;
}

@end

