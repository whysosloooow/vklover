//
//  DatabaseManager.m
//  VKLover
//
//  Created by admin on 13.11.17.
//

#import "DatabaseManager.h"
@class OVKCurrentUser;

@interface DatabaseManager ()

@property (strong, nonatomic) FIRDatabaseReference* reference;

@end

@implementation DatabaseManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        _reference = [[FIRDatabase database] reference];
    }
    return self;
}

+(id)sharedInstance {
    static DatabaseManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(void)saveCurrentUser {
    NSNumber* user_id = [OVKCurrentUser loadCurrentUser].user_id;
    [[_reference child:@"users"] observeSingleEventOfType:4 withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if(snapshot.value && [snapshot.value isKindOfClass:[NSNull class]]) {
            [[_reference child:@"users"] setValue:@{[user_id stringValue]:@{@"likes":@"",@"dislikes":@""}}];
        } else if(![snapshot.value valueForKey:[user_id stringValue]]) {
            [[_reference child:@"users"] updateChildValues:@{[user_id stringValue]:@{@"likes":@"",@"dislikes":@""}}];
        }
    }];
}

-(void)saveLikedUserId:(NSInteger)userId withDate:(NSString*)date {
    [[[[_reference child:@"users"] child:[[OVKCurrentUser loadCurrentUser].user_id stringValue]] child:@"likes"] updateChildValues:@{[NSString stringWithFormat:@"%ld",(long)userId]:date}];
}

- (void)loadLikesForCurrentUser:(OVKCurrentUser*)currentUser withBlock:(void(^)(NSDictionary*))completion {
    [[[[_reference child:@"users"] child:[currentUser.user_id stringValue]] child:@"likes"] observeSingleEventOfType:4 withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (completion) {
            completion(snapshot.value);
        }
    }];
}

-(void)saveDislikedUserId:(NSInteger)userId withDate:(NSString*)date {
    [[[[_reference child:@"users"] child:[[OVKCurrentUser loadCurrentUser].user_id stringValue]] child:@"dislikes"] updateChildValues:@{[NSString stringWithFormat:@"%ld",(long)userId]:date}];
}

-(void)loadDislikesForCurrentUser:(OVKCurrentUser*)currentUser withBlock:(void(^)(NSDictionary*))completion {
    [[[[_reference child:@"users"] child:[currentUser.user_id stringValue]] child:@"dislikes"] observeSingleEventOfType:4 withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (completion) {
            completion(snapshot.value);
        }
    }];
}

-(void)test {
    [_reference observeEventType:4 withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
         NSDictionary *postDict = snapshot.value;
    }];
}

-(void)checkMutualLikeWith:(NSNumber*)user_id completion:(void(^)(NSDictionary*))completion {
    [[[[_reference child:@"users"] child:[user_id stringValue]] child:@"likes"] observeSingleEventOfType:4 withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (completion) {
            completion(snapshot.value);
        }
    }];
}

@end
