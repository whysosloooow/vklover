//
//  UserDefaultsManager.m
//  VKLover
//
//  Created by admin on 02.11.17.
//

#import "UserDefaultsManager.h"

@interface UserDefaultsManager ()

@end

@implementation UserDefaultsManager

//Reading and writing from NSUserDefaults:
+ (void)saveCustomObject:(id)object key:(NSString *)key {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:key];
    [defaults synchronize];
}

+ (id)loadCustomObjectWithKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:key];
    return [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
}

+ (void)removeObjectWithKey:(NSString*)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:nil forKey:key];
    [defaults synchronize];
}


@end

