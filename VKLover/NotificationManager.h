//
//  NotificationManager.h
//  VKLover
//
//  Created by admin on 21.11.17.
//

#import <Foundation/Foundation.h>
#import <FirebaseMessaging/FirebaseMessaging.h>
#import <FirebaseInstanceID/FirebaseInstanceID.h>

@interface NotificationManager : NSObject

+(id)sharedInstance;
- (void) initiateRequestWithSubcribingToTopic:(NSString*)user_idTopic;

@end
