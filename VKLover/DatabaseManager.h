//
//  DatabaseManager.h
//  VKLover
//
//  Created by admin on 13.11.17.
//

#import <Foundation/Foundation.h>
#import <FirebaseDatabase/FirebaseDatabase/FIRDatabase.h>

#import "OVKCurrentUser.h"

@interface DatabaseManager : NSObject

+(id)sharedInstance;
-(void)saveDislikedUserId:(NSInteger)userId withDate:(NSString*)date;
-(void)loadDislikesForCurrentUser:(OVKCurrentUser*)currentUser withBlock:(void(^)(NSDictionary*))completion;
-(void)saveLikedUserId:(NSInteger)userId withDate:(NSString*)date;
-(void)loadLikesForCurrentUser:(OVKCurrentUser*)currentUser withBlock:(void(^)(NSDictionary*))completion;
-(void)saveCurrentUser;
-(void)checkMutualLikeWith:(NSNumber*)user_id completion:(void(^)(NSDictionary*))completion;

@end
