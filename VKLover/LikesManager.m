//
//  LikesManager.m
//  VKLover
//
//  Created by admin on 13.11.17.
//

#import "LikesManager.h"

@implementation LikesManager
static NSInteger countOfRatedFriends = 0;

+(id)sharedInstance {
    static DatabaseManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(void)saveLikeForUser:(NSInteger)user_id {
    NSDateFormatter *objDateFormatter = [[NSDateFormatter alloc] init];
    [objDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString* date = [objDateFormatter stringFromDate:[NSDate date]];
    [[DatabaseManager sharedInstance] saveLikedUserId:user_id withDate:date];
    id currentid = [@(user_id) stringValue];
    [[[LikesManager sharedInstance] likes] addObject:currentid];
    countOfRatedFriends++;
}

-(void)saveDisikeForUser:(NSInteger)user_id {
    NSDateFormatter *objDateFormatter = [[NSDateFormatter alloc] init];
    [objDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString* date = [objDateFormatter stringFromDate:[NSDate date]];
    [[DatabaseManager sharedInstance] saveDislikedUserId:user_id withDate:date];
    NSString *currentid = [@(user_id) stringValue];
    [[[LikesManager sharedInstance] dislikes] addObject:currentid];
    countOfRatedFriends++;
}

-(void)loadLikesWithCompletion:(void (^) (BOOL success))completion {
    [[DatabaseManager sharedInstance] loadLikesForCurrentUser:[OVKCurrentUser loadCurrentUser] withBlock:^(NSDictionary *likes) {
        if (![likes isKindOfClass:[NSNull class]] && [likes isKindOfClass:[NSDictionary class]]) {
        _likes = [NSMutableSet setWithArray:[likes allKeys]];
        countOfRatedFriends += [_likes count];
        } else {
            _likes = [[NSMutableSet alloc] init];
        }
        if (completion) {
            completion(YES);
        }
    }];
}

-(void)loadDislikesWithCompletion:(void (^) (BOOL success))completion {
    [[DatabaseManager sharedInstance] loadDislikesForCurrentUser:[OVKCurrentUser loadCurrentUser] withBlock:^(NSDictionary *dislikes) {
        if (![dislikes isKindOfClass:[NSNull class]]  && [dislikes isKindOfClass:[NSDictionary class]]) {
        _dislikes = [NSMutableSet setWithArray:[dislikes allKeys]];
        countOfRatedFriends += [_dislikes count];
        } else {
            _dislikes = [[NSMutableSet alloc] init];
        }
        if (completion) {
            completion(YES);
        }
    }];
}

-(BOOL)isLikedUserWithId:(NSInteger)user_id {
    return [_likes containsObject:[NSString stringWithFormat:@"%tu",user_id]];
}

-(BOOL)isDislikedUserWithId:(NSInteger)user_id {
    return [_dislikes containsObject:[NSString stringWithFormat:@"%tu",user_id]];
}


-(void)checkMutualLikeFor:(NSNumber*)user_id completion:(void (^) (BOOL success))completion {
    [[DatabaseManager sharedInstance] checkMutualLikeWith:user_id completion:^(NSDictionary *likes) {
        if(likes && [likes isKindOfClass:[NSNull class]]) {
            completion(NO);
        } else if([likes objectForKey:[[OVKCurrentUser loadCurrentUser].user_id stringValue]]) {
            completion(YES);
        } else {
            completion(NO);
        }
    }];
 }
@end
