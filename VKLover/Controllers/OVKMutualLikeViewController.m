//
//  OVKMutualLikeViewController.m
//  VKLover
//
//  Created by admin on 20.11.17.
//

#import "OVKMutualLikeViewController.h"
#import "OVKCurrentUser.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <CoreGraphics/CoreGraphics.h>

@interface OVKMutualLikeViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *myAvatar;
@property (weak, nonatomic) IBOutlet UIImageView *mutualAvatar;
@property (weak, nonatomic) IBOutlet UIImageView *heartImage;
@property (weak, nonatomic) IBOutlet UIButton *okButton;

@end

@implementation OVKMutualLikeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_myAvatar sd_setImageWithURL:[NSURL URLWithString:[OVKCurrentUser loadCurrentUser].photo_200]];
    [_mutualAvatar sd_setImageWithURL:[NSURL URLWithString:_mutualUrl]];
    _okButton.layer.cornerRadius = 24.;
}

- (IBAction)okButtonAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [UIView animateWithDuration:2.4 animations:^{
        _myAvatar.transform = CGAffineTransformRotate(_myAvatar.transform, -M_PI_4 / 5);
        _mutualAvatar.transform = CGAffineTransformRotate(_mutualAvatar.transform, M_PI_4 / 5);
        _heartImage.transform = CGAffineTransformMakeTranslation(self.view.frame.origin.x, self.view.frame.origin.x - 50);
    }];
}
@end
