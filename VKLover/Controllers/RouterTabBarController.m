//
// Created by admin on 23.11.17.
//

#import "RouterTabBarController.h"


@implementation RouterTabBarController

- (void)setNotificationURL:(NSURL *)url {
    _notificationURL = url;
    NSLog(@"[RouterTabBarController] url changed!");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"[RouterTabBarController] viewDidLoad loaded!");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

@end