//
//  OVKApi.m
//  iosvksdktest
//
//  Created by admin on 01.11.17.
//
#define APP_ID @"6233404"
#define OFFSET_PER_PAGE 10
#define VK_FIELDS_FOR_FRIENDS @"first_name, last_name, photo_100, sex, photo_200, city, last_seen, bdate"
#define VK_FIELDS_FOR_CURRENT_USER @"first_name, last_name, photo_100, sex, photo_200, city"
#define VK_COUNT_OF_FRIENDS_FOR_LOADING @"10"
#import "OVKApi.h"
#import "LikesManager.h"

@interface OVKApi() <VKSdkDelegate>
@end

@implementation OVKApi{
    void (^_getCurrentUserSuccess)(OVKCurrentUser *currentUser);
    void (^_getCurrentUserFailure)(NSError *error);
}
static OVKApi *sharedApi = nil;
static NSArray *SCOPE = nil;

+ (OVKApi *)sharedApi{
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedApi = [[OVKApi alloc] init];
        
    });
    return sharedApi;
}

- (id)init{
    if (self = [super init]) {
        
    }
    [[VKSdk initializeWithAppId:APP_ID] registerDelegate:self];
    return self;
}

- (void)wakeUpSessionWithSuccess:(OVKwakeUpSessionSucces)success and:(OVKFailure)failure {
    SCOPE = @[VK_PER_FRIENDS, VK_PER_EMAIL];
    
    [VKSdk wakeUpSession:SCOPE completeBlock:^(VKAuthorizationState state, NSError *error) {
        if (error) {
            NSLog(@"something happened");
            if (failure) {
                failure(error);
            }
            return;
        }
            if (success) {
                success(state);
            }
    }];
}

- (void)getFriendsWithPage:(NSInteger)page withSuccessBlock:(OVKgetFriendsSuccess)success andFailureBlock:(OVKFailure)failure {
    NSString *offsetInString;
    page = page * OFFSET_PER_PAGE;
    offsetInString = [NSString stringWithFormat:@"%ld",(long)page];
    
    VKRequest * getfriends = [[VKApi friends] get:@{VK_API_FIELDS :VK_FIELDS_FOR_FRIENDS , VK_API_ORDER: @"hints", VK_API_COUNT: VK_COUNT_OF_FRIENDS_FOR_LOADING, VK_API_OFFSET:offsetInString}];
    [getfriends executeWithResultBlock:^(VKResponse * response) {
        NSLog(@"Json result: %@", response.json);
        NSMutableArray *friends = [OVKUser createUsersWithResponseItems:response.json[@"items"]];
        NSInteger countOfAllFriends = 0;
        if ([response.json[@"count"] isKindOfClass:[NSNumber class]]) {
            countOfAllFriends = [response.json[@"count"] integerValue];
        }
        if (friends != nil) {
            friends = [[friends reverseObjectEnumerator] allObjects];
            if (success) {
                if ([offsetInString integerValue] + OFFSET_PER_PAGE > countOfAllFriends) {
                    success(friends, NO);
                } else {
                    success(friends, YES);
                }
            }
        }
    } errorBlock:^(NSError * error) {
        if (error.code != VK_API_ERROR) {
            [error.vkError.request repeat];
            
        }
        else {
            NSLog(@"VK error: %@", error);
            if (error != nil) {
                if (failure) {
                    failure(error);
                }
            }
        }
    }];
}

- (void)authorizeWithSuccess:(void(^)(OVKCurrentUser *currentUser))success andFailure:(void(^)(NSError *error))failure {
    _getCurrentUserSuccess = success;
    _getCurrentUserFailure = failure;
    [VKSdk authorize:SCOPE];
}

- (void)getPersonalDataWithSuccessBlock:(void(^)(NSDictionary *currentUser))success andFailureBlock:(void(^)(NSError *error))failure {
    VKRequest *personalInfo = [[VKApi users] get:@{VK_API_FIELDS : VK_FIELDS_FOR_CURRENT_USER}];
    [personalInfo executeWithResultBlock:^(VKResponse *response) {
        if (success) {
            success(response.json[0]);
        }
    } errorBlock:^(NSError *error) {
        if (failure){
            failure(error);
            [VKSdk forceLogout];
        }
    }];
}

- (void)getUsersbyIdsArray:(NSArray *)idsFromFB WithSuccessBlock:(void(^)(NSArray<OVKUser *> *loadedUsers))success andFailureBlock:(void(^)(NSError *error))failure {
    
    VKRequest *friendsByIdRequest = [[VKApi users] get:@{VK_API_USER_IDS: idsFromFB, VK_API_FIELDS: VK_FIELDS_FOR_FRIENDS}];
    [friendsByIdRequest executeWithResultBlock:^(VKResponse *response) {
        if (success) {
            success([OVKUser createUsersWithResponseItems:response.json]);
        }
    } errorBlock:^(NSError *error) {
        if (failure) {
            failure(error);
        }
            }];
}

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result {
    if(result.state == VKAuthorizationAuthorized || result.state == VKAuthorizationPending) {
        [self getPersonalDataWithSuccessBlock:^(NSDictionary *currentUser) {
            OVKCurrentUser* user = [[OVKCurrentUser alloc] initWithDictionary:currentUser andToken:result.token.accessToken];
            [user saveToUserDefaults];
            if (_getCurrentUserSuccess){
                _getCurrentUserSuccess(user);
            }
            _getCurrentUserSuccess = nil;
        } andFailureBlock:^(NSError *error) {
            NSLog(@"%@", [error localizedDescription]);
        }];
    } else {
        NSLog(@"Incorrect state for authorization!");
    }
}

- (void)vkSdkUserAuthorizationFailed {
    if (_getCurrentUserFailure){
        _getCurrentUserFailure(nil);
    }
    _getCurrentUserFailure = nil;
}

@end
