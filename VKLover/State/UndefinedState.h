//
//  UndefinedState.h
//  VKLover
//
//  Created by admin on 13.11.17.
//

#import <Foundation/Foundation.h>
#import "OVKState.h"
@interface UndefinedState : NSObject <OVKState>
- (id)init;
@end
