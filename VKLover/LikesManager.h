//
//  LikesManager.h
//  VKLover
//
//  Created by admin on 13.11.17.
//

#import <Foundation/Foundation.h>
#import "DatabaseManager.h"

@interface LikesManager : NSObject

@property (strong, nonatomic) NSMutableSet* likes;
@property (strong, nonatomic) NSMutableSet* dislikes;

+(id)sharedInstance;
-(void)saveLikeForUser:(NSInteger)user_id;
-(void)saveDisikeForUser:(NSInteger)user_id;
-(void)loadLikesWithCompletion:(void (^) (BOOL success))completion;
-(void)loadDislikesWithCompletion:(void (^) (BOOL success))completion;
-(BOOL)isLikedUserWithId:(NSInteger)user_id;
-(BOOL)isDislikedUserWithId:(NSInteger)user_id;
-(NSInteger)OffsetForRatedFiends;
-(void)checkMutualLikeFor:(NSNumber*)user_id completion:(void (^) (BOOL success))completion;

@end
