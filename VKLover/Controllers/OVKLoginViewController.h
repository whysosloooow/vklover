//
//  LoginViewController.h
//  VKLover
//
//  Created by admin on 03.11.17.
//

#import <UIKit/UIKit.h>
#import <VK-ios-sdk/VKSdk.h>
@interface OVKLoginViewController : UIViewController <VKSdkUIDelegate>

@end
