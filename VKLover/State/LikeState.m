//
//  LikeState.m
//  VKLover
//
//  Created by admin on 13.11.17.
//

#import "LikeState.h"

@implementation LikeState
- (id)init{
    if (self = [super init]) {
    }
    self.color = [UIColor colorWithRed:0.28 green:0.74 blue:0.40 alpha:1.0];
    return self;
}
@synthesize color;

@end
