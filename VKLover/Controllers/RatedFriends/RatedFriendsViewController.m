//
//  RatedFriendsViewController.m
//  VKLover
//
//  Created by admin on 13.11.17.
//

#import "RatedFriendsViewController.h"
#import "OVKFriendsFetcher.h"
#import "LikesManager.h"
#import "LikeState.h"
#import "IgnoreState.h"
#import "RatedFriendsViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MBProgressHUD.h"

@interface RatedFriendsViewController ()
@property(strong, nonatomic)NSArray* loadedFriendsWithFBAndVK;
@end

@implementation RatedFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.label.text = @"Загрузка";
    
    NSArray *likedAndDisliked = [[[[LikesManager sharedInstance] likes] allObjects] arrayByAddingObjectsFromArray:[[[LikesManager sharedInstance] dislikes] allObjects]];
    [[OVKApi sharedApi] getUsersbyIdsArray:likedAndDisliked  WithSuccessBlock:^(NSArray<OVKUser *> *loadedUsers) {
        _loadedFriendsWithFBAndVK = loadedUsers;
        [hud hideAnimated:YES];
        NSLog(@"%@", _loadedFriendsWithFBAndVK);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
        
    } andFailureBlock:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.tableView animated:YES];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.loadedFriendsWithFBAndVK count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RatedFriendsViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RatedFriendCell"];
    OVKUser *currentFriend = _loadedFriendsWithFBAndVK[indexPath.row];
    NSString *nameAndSurname = [currentFriend.first_name stringByAppendingString:@" "];
    nameAndSurname = [nameAndSurname stringByAppendingString:currentFriend.last_name];
    cell.nameLabel.text = nameAndSurname;
    cell.lastSeenLabel.text = currentFriend.last_seen;
    cell.coloredCircleBackgroundView.backgroundColor = currentFriend.state.color;
    [cell.avatarView bringSubviewToFront:cell.coloredCircleBackgroundView];
    [cell.friendImage sd_setImageWithURL:[NSURL URLWithString:currentFriend.photo_200]];
    if ([currentFriend.state.color isEqual:[UIColor colorWithRed:0.28 green:0.74 blue:0.40 alpha:1.0]]) {
        cell.isAppUserLabel.text = @"зарегистрирован в приложении";
    } else {
        cell.isAppUserLabel.text = @"не зарегистрирован в приложении";
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
