//
//  AppDelegate.m
//  VKLover
//
//  Created by admin on 01.11.17.
//

#import "AppDelegate.h"
#import "UserDefaultsManager.h"
#import "OVKCurrentUser.h"
#import "OVKFriendsViewController.h"
#import "OVKApi.h"
#import "RemoteConfig.h"
#import "DatabaseManager.h"
#import "OVKMutualLikeViewController.h"
#import "OVKLoginViewController.h"
#import "LikesManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
    [VKSdk processOpenURL:url fromApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]];
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [FIRApp configure];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
#endif
    }
    
    [application registerForRemoteNotifications];
    
    //Connect to messaging
    [FIRMessaging messaging].shouldEstablishDirectChannel= YES;
    
    [FIRMessaging messaging].delegate = self;
    
    return YES;
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    NSLog(@"%@", userInfo);
//    OVKMutualLikeViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]
//                                                   instantiateViewControllerWithIdentifier:@"OVKMutualLikeViewController"];
//    viewController.mutualUrl = [[userInfo objectForKey:@"notification"] objectForKey:@"body"];
//    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:viewController animated:YES completion:nil];
    // handle your message
    
    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(nonnull UILocalNotification *)notification {
    
}

- (void)messaging:(nonnull FIRMessaging *)messaging didReceiveMessage:(nonnull FIRMessagingRemoteMessage *)remoteMessage {
    NSLog(@"%@",remoteMessage.appData);
    OVKMutualLikeViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]
                                                   instantiateViewControllerWithIdentifier:@"OVKMutualLikeViewController"];
    viewController.mutualUrl = [[remoteMessage.appData objectForKey:@"notification"] objectForKey:@"body"];
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:viewController animated:YES completion:nil];
}

//логика загрузки вьюконтроллера в аппделегат
/////убрать анимацию
//перенаследовать таббар и следалть в нем метод открытия контроллера по пуш нотификации

@end
