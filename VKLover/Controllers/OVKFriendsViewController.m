//
//  FriendsViewController.m
//  VKLover
//
//  Created by admin on 03.11.17.
//

#import "OVKFriendsViewController.h"
#import "OVKCurrentUser.h"
#import "OVKFriendsFetcher.h"
#import "LikesManager.h"
#import "OVKMutualLikeViewController.h"
#import "NotificationManager.h"

@interface OVKFriendsViewController ()

@property(strong, nonatomic)NSMutableArray<OVKUser*>* friendsForLike;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityAndAgeLabel;
@property (weak, nonatomic) IBOutlet UIView *viewForSwipe;

@end

@implementation OVKFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _friendsForLike = [[NSMutableArray alloc] init];
    
    [[self navigationController] setNavigationBarHidden:YES];
    
    [self loadFriends];
}

-(void)loadFriends {
    [[OVKFriendsFetcher sharedFetcher] loadNextPageWithSuccessBlock:^(NSArray<OVKUser *> *loadedFriends){
        _friendsForLike = [NSMutableArray arrayWithArray:loadedFriends];
        if (loadedFriends != nil) {
            MDCSwipeToChooseViewOptions *options = [MDCSwipeToChooseViewOptions new];
            options.likedText = @"Like";
            options.likedColor = [UIColor blueColor];
            options.nopeText = @"Skip";
            options.delegate = self;
            options.onPan = ^(MDCPanState *state) {
                if (state.thresholdRatio == 1.f && state.direction == MDCSwipeDirectionLeft) {
                    //NSLog(@"delete");
                }
            };
            for (OVKUser *friend in loadedFriends) {
                MDCSwipeToChooseView *view = [[MDCSwipeToChooseView alloc] initWithFrame:self.viewForSwipe.bounds
                                                                                 options:options];
                view.backgroundColor = [UIColor clearColor];
                [view.imageView sd_setImageWithURL:[NSURL URLWithString:friend.photo_200]];
                view.clipsToBounds = YES;
                view.layer.cornerRadius = 20.;
                view.layer.borderWidth = 0.f;
                view.translatesAutoresizingMaskIntoConstraints = false;
                [self.viewForSwipe addSubview:view];
                [self.viewForSwipe addConstraint:[NSLayoutConstraint constraintWithItem:self.viewForSwipe
                                                                              attribute:NSLayoutAttributeTop
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:view
                                                                              attribute:NSLayoutAttributeTop
                                                                             multiplier:1
                                                                               constant:0]];
                [self.viewForSwipe addConstraint:[NSLayoutConstraint constraintWithItem:self.viewForSwipe
                                                                              attribute:NSLayoutAttributeLeading
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:view
                                                                              attribute:NSLayoutAttributeLeading
                                                                             multiplier:1
                                                                               constant:0]];
                [self.viewForSwipe addConstraint:[NSLayoutConstraint constraintWithItem:self.viewForSwipe
                                                                              attribute:NSLayoutAttributeTrailing
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:view
                                                                              attribute:NSLayoutAttributeTrailing
                                                                             multiplier:1
                                                                               constant:0]];
                [self.viewForSwipe addConstraint:[NSLayoutConstraint constraintWithItem:self.viewForSwipe
                                                                              attribute:NSLayoutAttributeBottom
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:view
                                                                              attribute:NSLayoutAttributeBottom
                                                                             multiplier:1
                                                                               constant:0]];
            }
            [self labelSet];
        } else {
            self.nameLabel.text = @"OOPS, no more friends...";
            self.cityAndAgeLabel.hidden = YES;
        }
    } andFailureBlock:^(NSError *error) {
        NSLog(@"Error");
    }];
}

- (IBAction)logoutAction:(UIButton *)sender {
    [VKSdk forceLogout];
    [OVKCurrentUser removeCurrentUser];
    [[OVKFriendsFetcher sharedFetcher] resetCurrentPage];
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *loginVC = [storyboard instantiateViewControllerWithIdentifier:@"login_vc_id"];
    
    [UIView transitionFromView:UIApplication.sharedApplication.delegate.window.rootViewController.view
                        toView:loginVC.view
                      duration:1
                       options:UIViewAnimationOptionTransitionFlipFromBottom
                    completion:^(BOOL finished) {
                        if(finished) {
                            NSLog(@"ANIMATION FOR LOGOUT FINISHED!");
                            UIApplication.sharedApplication.delegate.window.rootViewController = loginVC;
                        }
                    }];
}

// This is called then a user swipes the view fully left or right.
- (void)view:(UIView *)view wasChosenWithDirection:(MDCSwipeDirection)direction {
    [self saveActionWith:direction];
}

-(void) saveActionWith:(MDCSwipeDirection) direction {
    if (direction == MDCSwipeDirectionLeft) {
        if([[LikesManager sharedInstance] isDislikedUserWithId:[_friendsForLike.lastObject.user_id integerValue]]) {
            NSLog(@"this photo already disliked!");
        } else {
            [[LikesManager sharedInstance] saveDisikeForUser:[_friendsForLike.lastObject.user_id integerValue]];
        }
    } else {
        if([[LikesManager sharedInstance] isLikedUserWithId:[_friendsForLike.lastObject.user_id integerValue]]) {
            NSLog(@"this photo already liked!");
        } else {
            [[LikesManager sharedInstance] saveLikeForUser:[_friendsForLike.lastObject.user_id integerValue]];
            //mutual like
            //подписка на получение сообщений об изменениях для конкретного юзера
            [[NotificationManager sharedInstance] initiateRequestWithSubcribingToTopic:[_friendsForLike.lastObject.user_id stringValue]];
            NSString* copy_url = [_friendsForLike.lastObject.photo_200 copy];
            [[LikesManager sharedInstance] checkMutualLikeFor:_friendsForLike.lastObject.user_id  completion:^(BOOL success) {
                if(success) {
                    OVKMutualLikeViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]
                                                                   instantiateViewControllerWithIdentifier:@"OVKMutualLikeViewController"];
                    viewController.mutualUrl = copy_url;
                    [self presentViewController:viewController animated:YES completion:nil];
                }
            }];
        }
    }
    [_friendsForLike removeLastObject];
    if(_friendsForLike.count > 0) {
        [self labelSet];
    } else {
        [self.nameLabel setText:@""];
        [self.cityAndAgeLabel setText:@""];
        [self loadFriends];
    }
}

- (void) labelSet {
    [self.nameLabel setText:[NSString stringWithFormat:@"%@ %@", _friendsForLike.lastObject.first_name, _friendsForLike.lastObject.last_name]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.yyyy"];
    NSDate * birthday = [dateFormat dateFromString:_friendsForLike.lastObject.birthday];
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:birthday
                                       toDate:now
                                       options:0];
    NSInteger age = [ageComponents year];
    NSString* labelFormat;
    if([_friendsForLike.lastObject.city isEqualToString:@""]) {
        labelFormat = [NSString stringWithFormat:@""];
        if(age >= 1) {
            labelFormat = [labelFormat stringByAppendingString:[NSString stringWithFormat:@"%tu",age]];
        }
    } else {
        labelFormat = _friendsForLike.lastObject.city;
        if(age >= 1) {
            labelFormat = [labelFormat stringByAppendingString:[NSString stringWithFormat:@", %tu",age]];
        }
    }
    [self.cityAndAgeLabel setText:labelFormat];
}

- (IBAction)dislikeAction:(UIButton *)sender {
    [self.viewForSwipe.subviews.lastObject mdc_swipe:MDCSwipeDirectionLeft];
    OVKMutualLikeViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]
                                                   instantiateViewControllerWithIdentifier:@"OVKMutualLikeViewController"];
    viewController.mutualUrl = _friendsForLike[0].photo_200;
    [self presentViewController:viewController animated:YES completion:nil];
}

- (IBAction)likeAction:(UIButton *)sender {
    [self.viewForSwipe.subviews.lastObject mdc_swipe:MDCSwipeDirectionRight];
}
@end
