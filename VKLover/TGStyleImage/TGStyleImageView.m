//
//  TGStyleImage.m
//  VKLover
//
//  Created by admin on 20.11.17.
//

#import "TGStyleImageView.h"

@implementation TGStyleImageView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self customInit];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    [self customInit];
}

- (void)setNeedsLayout {
    [super setNeedsLayout];
    [self setNeedsDisplay];
}

- (void)prepareForInterfaceBuilder {
    [self customInit];
}

-(void)updateView{
    [self customInit];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self customInit];
}

-(void)customInit{
    if (_round)
        _cornerRadius = self.frame.size.height / 2;
    self.layer.cornerRadius = _cornerRadius;
    if (self.layer.cornerRadius > 0) {
        self.layer.masksToBounds = YES;
    }
    self.layer.borderWidth = _borderWidth;
    if (_borderColor) {
        self.layer.borderColor = _borderColor.CGColor;
    } else {
        self.layer.borderColor = self.backgroundColor.CGColor;
    }
}


@end
