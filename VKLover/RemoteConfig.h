//
//  RemoteConfig.h
//  VKLover
//
//  Created by admin on 09.11.17.
//

#import <Foundation/Foundation.h>

#import "OVKCurrentUser.h"


@interface RemoteConfig : NSObject

+(id)sharedInstance;

@end
