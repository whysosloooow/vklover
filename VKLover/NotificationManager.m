//
//  NotificationManager.m
//  VKLover
//
//  Created by admin on 21.11.17.
//

#import "NotificationManager.h"
#import "OVKCurrentUser.h"

@implementation NotificationManager


+(id)sharedInstance {
    static NotificationManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void) initiateRequestWithSubcribingToTopic:(NSString*)user_idTopic {
    [[FIRMessaging messaging] subscribeToTopic:[NSString stringWithFormat:@"%@+%@", [[OVKCurrentUser loadCurrentUser].user_id stringValue], user_idTopic]];
    //send post request
    //Content-Type: application/json
    //Authorization: key=AAAAnRYddGc:APA91bEm71LT57R0TeIFe7n2CIcoC5xTVfsbm-UEHXFqEk6hxhANiuJRiNCZfq5sff70EC9dZK9sfwwfBPkdOY7FgJ-x1iE97wakh6eyYUV1vMJlYU6YO_zRNGeSjZIqEoSyQoJm0i2y
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{@"Authorization": @"key=AIzaSyCER74_zH3Q5vIuoUAo3ZnKD-VC_wuwvV4",
                                                   @"Content-Type"  : @"application/json"
                                                   };
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSURL *url = [NSURL URLWithString:@"https://fcm.googleapis.com/fcm/send"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSString* topic = [NSString stringWithFormat:@"%@+%@", user_idTopic,[[OVKCurrentUser loadCurrentUser].user_id stringValue]];
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:@{
                                                                 @"to" : topic,
                                                                 @"priority" : @"high",
                                                                 @"notification" : @{
                                                                         @"body" : [OVKCurrentUser loadCurrentUser].photo_200 ,
                                                                         @"title" : topic,
                                                                         }
                                                                 }
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    /*
     {
     "to" : "12345",
     "priority" : "high",
     "notification" : {
     "body" : "12345+67890",
     "title" : "likes you",
     }
     }
     */
    request.HTTPMethod = @"POST";
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // The server answers with an error because it doesn't receive the params
        NSLog(@"message sended");
    }];
    [postDataTask resume];
}

@end
