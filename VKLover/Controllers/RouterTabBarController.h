//
// Created by admin on 23.11.17.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

@interface RouterTabBarController: UITabBarController
@property (nonatomic, strong) NSURL *notificationURL;
@end
